
// TimerDlg.h : header file
//

#pragma once

#include <gl\GL.h>
#include <gl\GLU.h>

// CTimerDlg dialog
class CTimerDlg : public CDialogEx
{
// Construction
public:
	CTimerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_TIMER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
	bool m_LBtDown;
	CPoint m_logMouse;
	bool m_isInitialized;
	bool m_exit;
	GLfloat m_rotate;


// Implementation
protected:
	void Render();
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	void OnOK();
	void OnCancel();
	afx_msg void OnWindowPosChanged(WINDOWPOS*);
	afx_msg void OnRButtonDblClk(UINT, CPoint);
	afx_msg void OnRButtonUp(UINT, CPoint);
	afx_msg void OnLButtonDown(UINT, CPoint);
	afx_msg void OnMouseMove(UINT, CPoint);
	afx_msg void OnLButtonUp(UINT, CPoint);
	afx_msg void OnKeyDown(UINT, UINT, UINT);
	DECLARE_MESSAGE_MAP()
};
