
// TimerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Timer.h"
#include "TimerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTimerDlg dialog




CTimerDlg::CTimerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTimerDlg::IDD, pParent),
	  m_LBtDown(false),
	  m_isInitialized(false),
	  m_exit(false),
	  m_rotate(0)
{
}

void CTimerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CTimerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_WINDOWPOSCHANGED()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// CTimerDlg message handlers

BOOL CTimerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO: Add extra initialization here
	CPaintDC dc(this);
	HDC hdc = dc.GetSafeHdc();
	PIXELFORMATDESCRIPTOR pfd;

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 16;
	pfd.cDepthBits = 16;
	pfd.iLayerType = PFD_MAIN_PLANE;

	int pf = ChoosePixelFormat(hdc, &pfd);
	SetPixelFormat(hdc, pf, &pfd);
	HGLRC hglrc = wglCreateContext(hdc);
	wglMakeCurrent(hdc, hglrc);

	glClearColor(0.5, 0.5, 0.5, 0.0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	this->Render();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTimerDlg::Render()
{
	CPaintDC dc(this); // device context for painting

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glLoadIdentity();
//	glTranslatef(renderX, renderY, 0.0);
//	glScalef(m_magnifyX, m_magnifyY, 1.0);
	glClearColor(0.5, 0.5, 0.5, 1.0f);

	glRotatef(m_rotate, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
	glEnd();

	/*
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glBindTexture(GL_TEXTURE_2D, texName);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, m_VertexData);
	glTexCoordPointer(2, GL_FLOAT, 0, m_TextureData);
	glNormalPointer(GL_FLOAT, 0, m_NormalData);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, (m_sliceX+1)*2*m_sliceY+2);

	glDisable(GL_TEXTURE_2D);
	*/

	glFlush();

	SwapBuffers(dc.GetSafeHdc());

	this->Invalidate();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTimerDlg::OnPaint()
{
	this->Render();
	m_rotate += 0.3f;
	if (m_rotate>=360.0f) m_rotate-=360.0f;
}

void CTimerDlg::OnWindowPosChanged(WINDOWPOS* lpwndpos)
{
	this->Render();
}

void CTimerDlg::OnRButtonDblClk(UINT nFlags, CPoint point)
{
	m_exit = true;
}

void CTimerDlg::OnRButtonUp(UINT nFlags, CPoint point)
{
	if (m_exit) this->DestroyWindow();
}

void CTimerDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_LBtDown = true;
	m_logMouse = point;
}

void CTimerDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if (!m_LBtDown) return;
	RECT pos;
	this->GetWindowRect(&pos);
	pos.left += point.x - m_logMouse.x;
	pos.right += point.x - m_logMouse.x;
	pos.top += point.y - m_logMouse.y;
	pos.bottom += point.y - m_logMouse.y;
	this->MoveWindow(&pos);
	this->Render();
}

void CTimerDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_LBtDown = false;
	this->Render();
}

void CTimerDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	TCHAR sz[10];
	_stprintf_s(sz, _T("%u"), nChar);
	this->MessageBox(sz);
}

void CTimerDlg::OnOK() {}

void CTimerDlg::OnCancel() {}